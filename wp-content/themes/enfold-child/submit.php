<?php
      $from = "info@juristu.nl"; 
      $to = $_POST['email']; // this is the author's email

      //Field values
        $factuurbedrag = $_POST['factuurbedrag'];
        $btw_plichtig  = $_POST['btw_plichtig'];
        $vervaldatum   = $_POST['vervaldatum'];
        $tel           = $_POST['tel'];
        $debiteur      = $_POST['debiteur'];
        $total         = $_POST['total'];

      $subject = "Bereken incasso kosten"; 
      $message = '
        <table width="99%" border="0" cellpadding="1" cellspacing="0" bgcolor="#EAEAEA"><tbody><tr><td>
            <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
                    <tbody><tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong>Factuurbedrag</strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px">'. $factuurbedrag . '</font>
                        </td>
                    </tr>
                    <tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong></strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px"><ul class="m_5027344731308698833bulleted"><li>'. $btw_plichtig . '</li></ul></font>
                        </td>
                    </tr>
                    <tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong>Vervaldatum van de factuur</strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px">'. $vervaldatum . '</font>
                        </td>
                    </tr>
                    <tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong>Uw telefoonummer</strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px">'. $tel . '</font>
                        </td>
                    </tr>
                    <tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong>De debiteur is een:</strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px">'. $debiteur . '</font>
                        </td>
                    </tr>
                    <tr bgcolor="#EAF2FA">
                        <td colspan="2">
                            <font style="font-family:sans-serif;font-size:12px"><strong>totale-kosten</strong></font>
                        </td>
                    </tr>
                    <tr bgcolor="#FFFFFF">
                        <td width="20">&nbsp;</td>
                        <td>
                            <font style="font-family:sans-serif;font-size:12px">' . $total . '</font>
                        </td>
                    </tr>
                    </tbody></table>
                    </td>
                </tr>
            </tbody>
        </table>
      ';

      $headers  = "MIME-Version: 1.0\r\n";
      $headers .= "Content-type: text/html; charset=UTF-8\r\n";
      $headers .= "From:" . $from;

      mail($to,$subject,$message, $headers);
?>