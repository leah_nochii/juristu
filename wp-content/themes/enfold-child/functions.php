<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file.
* Wordpress will use those functions instead of the original functions then.
*/
//set builder mode to debug
/*
add_action('avia_builder_mode', "builder_set_debug");
function builder_set_debug()
{
	return "debug";
}
*/
//End set builder mode to debug

// WEBDELTA medewerker functienaliteit
if (!session_id()) {
    session_start();
}
if ($_GET['medewerker']) {
    $_SESSION['medewerker'] = strip_tags($_GET['medewerker']);
}
/*
incasso 			> medewerker ID = fld_7374118_1
meerdere incasso's 	> medewerker ID = fld_7851808_1
*/
function medewerker_func() {
    if (!empty($_SESSION['medewerker'])) {
        if ( strpos($_SERVER[REQUEST_URI], 'nu-incasseren') || strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen') ) {
//			$w3_plugin_totalcache->flush_pgcache();
            echo '<script>';
            echo 'var $j = jQuery.noConflict();$j(document).ready(function() {';
            if (strpos($_SERVER[REQUEST_URI], 'nu-incasseren')) {
                echo '$j("input[name=\'medewerker\']").val("'.$_SESSION['medewerker'].'");';
				// Gravity forms is gebruikt icm Contactform7
                echo '$j("input[name=\'input_63\']").val("'.$_SESSION['medewerker'].'");';
            }
            if (strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen')) {
                echo '$j("#fld_7851808_1").val("'.$_SESSION['medewerker'].'");';
				// Gravity forms is gebruikt icm Contactform7
                echo '$j("input[name=\'input_63\']").val("'.$_SESSION['medewerker'].'");';
            }
            echo '});';
            echo '</script>';
        }
    }
}
add_shortcode( 'medewerker', 'medewerker_func' );

function add_js_functions(){
    ?>
    <script>
        jQuery(document).ready(function($) {
            //$('#vervaldatum').attr('autocomplete', 'off');
            $('#input_5_12').attr('autocomplete', 'off');

            $('#bereken').on( 'click' , function(e) {
                e.preventDefault();
                var min = 40;
                var max = 6675;

                //var bedrag = $('#bedrag').val();
                var bedrag = $('#input_5_10').val();
                var btwPlichtig = false;
                //var date = $('#vervaldatum').val();
                var date = $('#input_5_12').val();
                var todayDate = new Date();
                var today = todayDate.toLocaleDateString();
                var debiteur = "";

                //if ($('#bedrijf').is(':checked')) {
                if ($('#choice_5_13_0').is(':checked')) {   
                    debiteur = "bedrijf";
                } else {
                    debiteur = "consument";
                }

                // if ($('#btw').is(':checked')) {
                if ($('#choice_5_11_1').is(':checked')) {
                    btwPlichtig = true;
                }
                var telco  = $( '#input_5_16' );
                intRegex = /[0-9 -()+]+$/;
                if(telco.val() && intRegex.test(telco.val()) === true && telco.val().length > 6){

                    getAjax(bedrag, debiteur, btwPlichtig, date, today);

                    if(telco.parent().parent().hasClass('gfield_error') === true){
                            telco.siblings().remove('.validation_message');
                            telco.parent().parent().removeClass('gfield_error');
                    }

                    window.setTimeout(function () {
                    //Form fields IDs
                    var factuurbedrag = $( '#input_5_10' ).val();
                    var btw_plichtig  = $( '#choice_5_11_1' ).val();
                    var vervaldatum   = $( '#input_5_12' ).val();
                    var tel           = $( '#input_5_16' ).val();
                    var debiteur      = $( 'input[name="input_13"]:checked' ).val();
                    var total         = $( "#totale-kosten" ).val();
                    
                        //Send email notification if calculation button was clicked
                        SubmitData( factuurbedrag, btw_plichtig, vervaldatum, tel, debiteur, total );
                    }, 2000);

                }else{

                    if(telco.parent().parent().hasClass('gfield_error') === false){
                        telco.parent().append('<div class="gfield_description validation_message">'+(telco.val().length <= 0 ? 'Dit veld is verplicht.' : 'Must be a valid telephone number.' )+'</div>').parent().addClass('gfield_error');
                    }else{
                        telco.siblings('.validation_message').html(telco.val().length <= 0 ? 'Dit veld is verplicht.' : 'Must be a valid telephone number.' );
                    }
                    
                }

            });

             function SubmitData( factuurbedrag, btw_plichtig, vervaldatum, tel, debiteur, total  ) {
                // alert(  factuurbedrag + btw_plichtig + vervaldatum + tel + debiteur + total  );
                var submit_url = "/wp-content/themes/enfold-child/submit.php";
                var email = 'info@juristu.nl,maurits@juristu.nl,klantenservice@juristu.nl';
                
                $.post( submit_url, { email: email, factuurbedrag: factuurbedrag, btw_plichtig: btw_plichtig, vervaldatum: vervaldatum, tel: tel, debiteur: debiteur, total: total });
                
            }

            function getAjax(factuurBedrag, debiteur, btw, vervaldatum, vandaag) {
                $.ajax({
                    type:"POST",
                    url: "/wp-content/themes/enfold-child/berekenincasso.php",
                    data: {bedrag: factuurBedrag, debiteur: debiteur, btw: btw, vervaldatum: vervaldatum, vandaag: vandaag },
                    success: function(response) {
                        $("#outputBerekening").html(response);
                        var total_kosten = $( "#totale-kosten" ).val();
                        $( "#input_5_15" ).val( total_kosten );
                        $('#gform_submit_button_5').show();
                        dataLayer.push({ 
                            'event' : 'GAEvent', 
                            'eventCategory' : 'Form', 
                            'eventAction' : 'Submit', 
                            'eventLabel' : 'Incassokosten berekenen', 
                            'eventValue' : undefined 
                        });
                    }
                });
            }

            $('.ginput_container_phone input').attr('type','tel');
            $('.input--number input').attr('type', 'number');

            $(document).on('gform_page_loaded', function(event, form_id, current_page){
                $('.ginput_container_phone input').attr('type','tel');
                $('.input--number input').attr('type', 'number');
            });
        });

    </script>
    <?php
}
add_action('wp_head','add_js_functions');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/css/custom.css',
        array('parent-style')
    );
} 

function get_mime_type($ext) {
    if ($ext === 'jpg' || $ext === 'png' || $ext === 'jpeg') {
        $ext === 'jpg' ? $type = 'image/jpeg' : $type = 'image/' . $ext;
    } elseif ($ext === 'pdf' || $ext === 'xls' || $ext === 'xlsx' || $ext === 'doc' || $ext === 'docx' || $ext === 'odt' || $ext === 'ods' || $ext === 'map' || $ext === 'msg') {
        $app = 'application/';

        if ($ext === 'pdf') {
            $type = $app . 'pdf';
        } elseif ($ext === 'xls') {
            $type = $app . 'vnd.ms-excel';
        } elseif($ext === 'xlsx') {
            $type = $app . 'vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        } elseif($ext === 'doc') {
            $type = $app . 'msword';
        } elseif($ext === 'docx') {
            $type = $app . 'vvnd.openxmlformats-officedocument.wordprocessingml.document';
        } elseif($ext === 'odt') {
            $type = $app . 'vnd.oasis.opendocument.text';
        } elseif($ext === 'ods') {
            $type = $app . 'vnd.oasis.opendocument.spreadsheet';
        } elseif($ext === 'map') {
            $type = $app . 'x-httpd-imap';
        }  else {
            $type = $app . 'vnd.ms-outlook';
        }
    } elseif ($ext === 'eml') {
        $type = 'message/rfc822';
    }

    return $type;
}


add_action( 'gform_after_submission_4', 'post_to_third_party_4', 10, 2 );
function post_to_third_party_4( $entry, $form ) {
    $companyName = rgar( $entry, '2' );
    $firstName = rgar( $entry, '4' );
    $lastName = rgar( $entry, '5' );
    
    $companyName2 = rgar( $entry, '17' );
    $name = rgar( $entry, '18' );
    $address = rgar( $entry, '22' );
    $postcode = rgar( $entry, '23' );
    $residence = rgar( $entry, '24' );
    $number = rgar( $entry, '20' );
    $mobile = rgar( $entry, '21' );
    $email = rgar( $entry, '19' );

    $invoiceNum = rgar( $entry, '27' );
    $rawDate = explode('-', rgar( $entry, '62' ));
    $date = $rawDate[1] . '-' . $rawDate[2] . '-' .$rawDate[0];
    $expiration = rgar( $entry, '30' );
    $invoiceAmount = rgar( $entry, '28' );
    $message = rgar( $entry, '31' );

    // First page defaults
    
    if ($companyName === '' || $companyName === ' '  || !$companyName) {
        $companyName = $firstName . ' ' . $lastName;
    }

    // Second page defaults
    
    if ($companyName2 === '' || $companyName2 === ' ' || !$companyName2) {
        $companyName2 = 'Geen bedrijfsnaam';
    }
    
    if ($name === '' || $name === ' ' || !$name) {
        $name = 'Onbekend';
    }
    
    if ($address === '' || $address === ' ' || !$address) {
        $address = 'Onbekend';
    }
    
    if ($postcode === '' || $postcode === ' ' || !$postcode) {
        $postcode = 'Onbekend';
    }
    
    if ($residence === '' || $residence === ' ' || !$residence) {
        $residence = 'Onbekend';
    }
    
    if ($number === '' || $number === ' ' || !$number) {
        $number = 'Onbekend';
    }
    
    if ($mobile === '' || $mobile === ' ' || !$mobile) {
        $mobile = 'Onbekend';
    }
    
    if ($email === '' || $email === ' ' || !$email) {
        $email = 'Onbekend';
    }

    // Third page defaults
    
    if ($invoiceNum === '' || $invoiceNum === ' ' || !$invoiceNum) {
        $invoiceNum = 'Openstaand';
    }
    
    if ($expiration === '' || $expiration === ' ' || !$expiration) {
        $expiration = '2019-01-01';
    }
    
    if ($invoiceAmount === '' || $invoiceAmount === ' ' || !$invoiceAmount) {
        $invoiceAmount = 'Openstaand';
    }
    
    if ($message === '' || $message === ' ' || !$message) {
        $message = 'Onbekend';
    }

    $raw = rgar( $entry, '32' );
    $raw = str_replace( "[", "", $raw );
    $raw = str_replace( "]", "", $raw );
    $raw = explode( ",", $raw );

    $files = [];
    $count = 1;
    foreach( $raw as $file ) {
        GFCommon::log_debug( 'CONSOLE.LOG("SINGLE_FILE")'. $file );
        $path = substr($file, 1, -1);
        GFCommon::log_debug( 'CONSOLE.LOG("SINGLE_FILE_PATH")'. $path );
        $path = str_replace('\\', '', $path);
        $file_name = basename($path);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
        
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
        
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
        
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
        
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
        
            // ms office
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'ppt' => 'application/vnd.ms-powerpoint',
            'eml' => 'message/rfc822',
        
            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        
        $ext = strtolower(array_pop(explode('.',$file_name)));
        if (array_key_exists($ext, $mime_types)) {
            $type =  $mime_types[$ext];
        }
        
        //LIVE
        $filetoReadhttps = site_url()."/";
        $path = str_replace($filetoReadhttps,"",$path);

        //STAGING
        // $filetoReadnonhttps = str_replace( "https:", "http:", site_url()."/" );
        // $path = str_replace( $filetoReadnonhttps, "", $path );

        GFCommon::log_debug( 'CONSOLE.LOG("PATH")'.$path );
        // end Update
        $data = file_get_contents($path);
        GFCommon::log_debug( 'CONSOLE.LOG("DATA")'.$data );
        $base64 = base64_encode($data);
        GFCommon::log_debug( 'CONSOLE.LOG("BASE64")'.$base64 );
        
        if ($file_name === '' || $file_name === ' ' || !$file_name) {
            $file_name = 'Onbekend';
        }
        
        if ($type === '' || $type === ' ' || !$type) {
            $type = 'Onbekend';
        }
        
        if ($base64 === '' || $base64 === ' ' || !$base64) {
            $base64 = 'Onbekend';
        }

        $new_array = array( "naam" => $file_name, "content-type" => $type, "data" => $base64);
        GFCommon::log_debug( 'CONSOLE.LOG("new_array")'. print_r( $new_array, true ) );
        array_push( $files, $new_array );
        $count++;
    } //END Foreach on files
 

    //GFCommon::log_debug( 'CONSOLE.LOG("files_array")'. print_r( $files, true ) );

    $post_url = 'https://juristu.yazoom.nl/ws/';
    $body = json_encode(array(
        "key" => rgar( $entry, '61' ),
        "og" => array(
            "bedrijfsnaam" => $companyName,
            "voornaam" => $firstName,
            "achternaam" => $lastName,
            "adres" => rgar( $entry, '10' ),
            "postcode" => rgar( $entry, '11' ),
            "woonplaats" => rgar( $entry, '12' ),
            "telefoon" => rgar( $entry, '8' ),
            "email" => rgar( $entry, '7' )
        ),
        "deb" => array(
            "bedrijfsnaam" => $companyName2,
            "naam" => $name,
            "adres" => $address,
            "postcode" => $postcode,
            "woonplaats" => $residence,
            "telefoon" => $number,
            "mobiel" => $mobile,
            "email" => $email
        ),
        "vordering" => array(
            "factuurnummer" => $invoiceAmount,
            "factuurdatum" => $expiration,
            "vervaldatum" => $expiration,
            "factuurbedrag" => $invoiceNum,
            "toelichting" => $message,
            "bijlage" => $files,
        )
    ));
    // GFCommon::log_debug( '$date => ' . print_r( $date, true ) );
    // GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    // GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}

add_action( 'gform_after_submission_7', 'post_to_third_party_7', 10, 2 );
function post_to_third_party_7( $entry, $form ) {
    $companyName = rgar( $entry, '3' );
    $firstName = rgar( $entry, '4' );
    $lastName = rgar( $entry, '5' );

    $companyName2 = rgar( $entry, '22' );
    $name = rgar( $entry, '23' );
    $address = rgar( $entry, '33' );
    $postcode = rgar( $entry, '34' );
    $residence = rgar( $entry, '35' );
    $number = rgar( $entry, '28' );
    $mobile = rgar( $entry, '29' );
    $email = rgar( $entry, '24' );

    $invoiceNum = rgar( $entry, '42' );
    $rawDate = explode('-', rgar( $entry, '56' ));
    $date = $rawDate[1] . '-' . $rawDate[2] . '-' .$rawDate[0];
    $expiration = rgar( $entry, '54' );
    $invoiceAmount = rgar( $entry, '43' );
    $message = rgar( $entry, '49' );

    // First page defaults
    
    if ($companyName === '' || $companyName === ' '  || !$companyName) {
        $companyName = $firstName . ' ' . $lastName;
    }

    // Second page defaults
    
    if ($companyName2 === '' || $companyName2 === ' ' || !$companyName2) {
        $companyName2 = 'Geen bedrijfsnaam';
    }
    
    if ($name === '' || $name === ' ' || !$name) {
        $name = 'Onbekend';
    }
    
    if ($address === '' || $address === ' ' || !$address) {
        $address = 'Onbekend';
    }
    
    if ($postcode === '' || $postcode === ' ' || !$postcode) {
        $postcode = 'Onbekend';
    }
    
    if ($residence === '' || $residence === ' ' || !$residence) {
        $residence = 'Onbekend';
    }
    
    if ($number === '' || $number === ' ' || !$number) {
        $number = 'Onbekend';
    }
    
    if ($mobile === '' || $mobile === ' ' || !$mobile) {
        $mobile = 'Onbekend';
    }
    
    if ($email === '' || $email === ' ' || !$email) {
        $email = 'Onbekend';
    }

    // Third page defaults
    
    if ($invoiceNum === '' || $invoiceNum === ' ' || !$invoiceNum) {
        $invoiceNum = 'Openstaand';
    }
    
    if ($expiration === '' || $expiration === ' ' || !$expiration) {
        $expiration = '2019-01-01';
    }
    
    if ($invoiceAmount === '' || $invoiceAmount === ' ' || !$invoiceAmount) {
        $invoiceAmount = 'Openstaand';
    }
    
    if ($message === '' || $message === ' ' || !$message) {
        $message = 'Onbekend';
    }

    $raw = rgar( $entry, '52' );
    $raw = str_replace( "[", "", $raw );
    $raw = str_replace( "]", "", $raw );
    $raw = explode( ",", $raw );

    $files = [];
    $count = 1;
    foreach( $raw as $file ) {
        GFCommon::log_debug( 'CONSOLE.LOG("SINGLE_FILE")'. $file );
        $path = substr($file, 1, -1);
        GFCommon::log_debug( 'CONSOLE.LOG("SINGLE_FILE_PATH")'. $path );
        $path = str_replace('\\', '', $path);
        $file_name = basename($path);
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',
        
            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',
        
            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',
        
            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
        
            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',
        
            // ms office
            'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'xlsx' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'ppt' => 'application/vnd.ms-powerpoint',
            'eml' => 'message/rfc822',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
        
        $ext = strtolower(array_pop(explode('.',$file_name)));
        if (array_key_exists($ext, $mime_types)) {
            $type =  $mime_types[$ext];
        }
        
        //LIVE
        $filetoReadhttps = site_url()."/";
        $path = str_replace($filetoReadhttps,"",$path);

        //STAGING
        // $filetoReadnonhttps = str_replace( "https:", "http:", site_url()."/");
        // $path = str_replace( $filetoReadnonhttps, "", $path );

        GFCommon::log_debug( 'PATH FILE'.$path );
        $data = file_get_contents($path);
        $base64 = base64_encode($data);
        
        if ($file_name === '' || $file_name === ' ' || !$file_name) {
            $file_name = 'Onbekend';
        }
        
        if ($type === '' || $type === ' ' || !$type) {
            $type = 'Onbekend';
        }
        
        if ($base64 === '' || $base64 === ' ' || !$base64) {
            $base64 = 'Onbekend';
        }

        $new_array = array( "naam" => $file_name, "content-type" => $type, "data" => $base64);
        GFCommon::log_debug( 'CONSOLE.LOG("new_array")'. print_r( $new_array, true ) );
        array_push( $files, $new_array );
        $count++;
    } //END foreach
 
    $post_url = 'https://juristu.yazoom.nl/ws/';
    $body = json_encode(array(
        "key" => rgar( $entry, '55' ),
        "og" => array(
            "bedrijfsnaam" => $companyName,
            "voornaam" => $firstName,
            "achternaam" => $lastName,
            "adres" => rgar( $entry, '11' ),
            "postcode" => rgar( $entry, '15' ),
            "woonplaats" => rgar( $entry, '16' ),
            "telefoon" => rgar( $entry, '10' ),
            "email" => rgar( $entry, '9' )
        ),
        "deb" => array(
            "bedrijfsnaam" => $companyName2,
            "naam" => $name,
            "adres" => $address,
            "postcode" => $postcode,
            "woonplaats" => $residence,
            "telefoon" => $number,
            "mobiel" => $mobile,
            "email" => $email
        ),
        "vordering" => array(
            "factuurnummer" => $invoiceAmount,
            "factuurdatum" => $expiration,
            "vervaldatum" => $expiration,
            "factuurbedrag" => $invoiceNum,
            "toelichting" => $message,
            "bijlage" => $files,
        )
    ));
    GFCommon::log_debug( '$date => ' . print_r( $date, true ) );
    GFCommon::log_debug( 'gform_after_submission: body => ' . print_r( $body, true ) );
 
    $request = new WP_Http();
    $response = $request->post( $post_url, array( 'body' => $body ) );
    GFCommon::log_debug( 'gform_after_submission: response => ' . print_r( $response, true ) );
}
add_filter( 'gform_validation_message', 'change_error_message', 10, 2 );
function change_error_message( $message ) {
    return "<div class='validation_error'>Er was een probleem met uw applicatie</div>";
}

add_action('wp_footer', 'sticky_btns');
function sticky_btns() {
    if ( is_active_sidebar( 'Sticky Buttons' ) ) : ?>
    <?php dynamic_sidebar( 'Sticky Buttons' ); 
    endif;
}