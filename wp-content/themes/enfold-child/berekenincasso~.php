<?php

$bedrag = $_POST['bedrag'];
$debiteur = $_POST['debiteur'];
$btw = $_POST['btw'];
$verval = $_POST['vervaldatum'];
$vandaag = $_POST['vandaag'];

bereken_bedrag($bedrag, $debiteur, $btw, $verval, $vandaag);

function bereken_bedrag($bedrag, $debiteur, $btwPlichtig, $vervaldatum)
{
	if ($debiteur == "consument") {
		$rentePercentage = 0.02;
	} else {
		$rentePercentage = 0.08;
	}

	$factuurkosten = $bedrag;
	$verval = new DateTime($vervaldatum);
	$vandaag = new DateTime();

    $kosten = 0.00;
	$eerste = 0.00;
	$tweede = 0.00;
	$derde = 0.00;
	$vierde = 0.00;
	$vijfde = 0.00;
    if ($bedrag <= 2500) {
        $kosten += $bedrag * 0.15;
        $eerste = $kosten;
        if ($kosten < 40) {
        	$eerste = 40;
            $kosten = 40;
        }
    }
    if ($bedrag >= 2501 && $bedrag <= 5000) {
    	$eerste = 375;
        $kosten += 375;


        $bedrag2 = $bedrag - 2500;
        $kosten2 = $bedrag2 * 0.10;
        $kosten = $kosten + $kosten2;
        $tweede = $kosten2;
    }
	if ($bedrag >= 5001 && $bedrag <= 10000) {
		$eerste = 375;
		$tweede = 250;
		$kosten += $eerste + $tweede;


		$bedrag2 = $bedrag - 5000;
		$kosten2 = $bedrag2 * 0.05;
		$kosten = $kosten + $kosten2;
		$derde = $kosten2;
	}
    if ($bedrag >= 10001 && $bedrag <= 190000) {
    	$eerste = 375;
    	$tweede = 250;
    	$derde = 250;
        $kosten = 375 + 250 + 250;


        $bedrag2 = $bedrag - 10000;
//        $bedrag = $bedrag - 5000;
        $kosten2 = $bedrag2 * 0.01;
        $kosten = $kosten + $kosten2;
        $vierde = $kosten2;
    }
    if ($bedrag >= 190001 && $bedrag <= 800000) {
    	$eerste = 375;
    	$tweede = 250;
    	$derde = 250;
    	$vierde = 1900;
        $kosten = 375 + 250 + 250 + 1900;


        $bedrag2 = $bedrag - 200000;
        $kosten2 = $bedrag2 * 0.005;
        $kosten = $kosten + $kosten2;
        $vijfde  = $kosten2;
    }
    if ($bedrag >= 800001) {
	    $eerste = 375;
	    $tweede = 250;
	    $derde = 250;
	    $vierde = 1900;
	    $vijfde = 4000;
		$kosten = 375 + 250 + 250 + 1900;

        $bedrag2 = $bedrag - 200000;
        $kosten2 = $bedrag2 * 0.005;
        $kosten = $kosten + $kosten2;
        $vijfde = $kosten2;

        if ($kosten > 6775) {
            $kosten = 6775;
            $vijfde = 4000;
        }
    }

	$kosten = round($kosten, 2);

	if ($btwPlichtig == "false") {
		$btw = $kosten * 0.21;
		$kosten = round($kosten + $btw, 2);
	}

	$dagen = $vandaag->diff($verval)->format("%a");

    $rente = $factuurkosten * $dagen * $rentePercentage / 365;

    $totaal =  $bedrag + $kosten + $rente;

	$bedrag = number_format((float)$bedrag, 2, ',', '.');
	$rente = number_format((float)$rente, 2, ',', '.');
	$kosten = number_format((float)$kosten, 2 , ',', '.');
	$totaal = number_format((float)$totaal, 2 , ',', '.');

    echo '<div class="incassokosten">';
    echo '<table style="width:50%">';
	echo '<tr><td>Oorspronkelijke factuur</td><td>€' . htmlspecialchars($bedrag) .  '</td></tr></br>';
    echo '<tr><td><b>Totale incassokosten</b></td><td>€'. $kosten .  ' </td></tr></br>';
    echo '<tr class="tablerow"><td><d iv class="borderline">Rente</td><td>€' . $rente .  '</div></td></tr>';
    echo '<hr>';
    echo '<tr><td><b>Totale vordering</b></td><td>€' . $totaal .  '</td></td></tr>';
    echo '</table>';
    echo '</div>';

    echo '<div class="table2">';
    echo '<table style="width: 50%">';
    echo '<b>Incassokosten</b></br>';
    echo '<p>De incassokosten zijn berekend aan de hand van onderstaande staffel:</p>';
    echo '<tr><td>Over de eerste</td><td>€2.500,00</td><td>15%</td><td>€' . htmlspecialchars($eerste) .  '</td></tr>';
    echo '<tr><td>Over de volgende</td><td>€2.500,00</td><td>10%</td><td>€' . htmlspecialchars($tweede) .  '</td></tr>';
    echo '<tr><td>Over de volgende</td><td>€5.000,00</td><td>5%</td><td>€' . htmlspecialchars($derde) .  '</td></tr>';
    echo '<tr><td>Over de volgende</td><td>€190.000,00</td><td>1%</td><td>€' . htmlspecialchars($vierde) .  '</td></tr>';
    echo '<tr><td>Over de volgende</td><td>€800.000,00</td><td>0,5%</td><td>€' . htmlspecialchars($vijfde) . '</td></tr>';
    echo '</table>';
    echo '</div>';
    echo '<b>Rente</b></br>';
    echo '<p class="centertext">De rente is berekend op basis van de wettelijke rente voor consumententransacties. 
          Houd er rekening mee dat deze berekening een benadering is! Het daadwerkelijke rentebedrag is afhankelijk van verschillende factoren,
          zoals het aantal facturen, deelbetalingen, algemene voorwaarden, etc.</p>';
}

?>

