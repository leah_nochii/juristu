<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file. 
* Wordpress will use those functions instead of the original functions then.
*/
//set builder mode to debug
/*
add_action('avia_builder_mode', "builder_set_debug");
function builder_set_debug()
{
	return "debug";
}
*/
//End set builder mode to debug

// WEBDELTA medewerker functienaliteit
if (!session_id()) {
    session_start();
}
if ($_GET['medewerker']) {
    $_SESSION['medewerker'] = strip_tags($_GET['medewerker']);
}
/*
incasso 			> medewerker ID = fld_7374118_1
meerdere incasso's 	> medewerker ID = fld_7851808_1
*/
function medewerker_func() {
	if (!empty($_SESSION['medewerker'])) {
		if ( strpos($_SERVER[REQUEST_URI], 'nu-incasseren') || strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen') ) {
			echo '<script>';
			echo 'var $j = jQuery.noConflict();$j(document).ready(function() {';
			if (strpos($_SERVER[REQUEST_URI], 'nu-incasseren')) {
				echo '$j("#fld_7374118_1").val("'.$_SESSION['medewerker'].'");';
			}
			if (strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen')) {
				echo '$j("#fld_7851808_1").val("'.$_SESSION['medewerker'].'");';
			}
			echo '});';
			echo '</script>';
		}
	}
}
add_shortcode( 'medewerker', 'medewerker_func' );
