<?php

$bedrag = $_POST['bedrag'];
$debiteur = $_POST['debiteur'];
$btw = $_POST['btw'];
$verval = $_POST['vervaldatum'];
$vandaag = $_POST['vandaag'];

bereken_bedrag($bedrag, $debiteur, $btw, $verval, $vandaag);

function bereken_bedrag($bedrag, $debiteur, $btwPlichtig, $vervaldatum)
{
	if ($debiteur == "consument") {
		$rentePercentage = 0.02;
	} else {
		$rentePercentage = 0.08;
	}

	$factuurkosten = $bedrag;
	$verval = new DateTime($vervaldatum);
	$vandaag = new DateTime();

    $kosten = 0.00;
	$eerste = 0.00;
	$tweede = 0.00;
	$derde = 0.00;
	$vierde = 0.00;
	$vijfde = 0.00;
    if ($bedrag <= 2500) {
        $kosten += $bedrag * 0.15;
        $eerste = $kosten;
        if ($kosten < 40) {
        	$eerste = 40;
            $kosten = 40;
        }
    }
    if ($bedrag >= 2501 && $bedrag <= 5000) {
    	$eerste = 375;
        $kosten += 375;


        $bedrag2 = $bedrag - 2500;
        $kosten2 = $bedrag2 * 0.10;
        $kosten = $kosten + $kosten2;
        $tweede = $kosten2;
    }
	if ($bedrag >= 5001 && $bedrag <= 10000) {
		$eerste = 375;
		$tweede = 250;
		$kosten += $eerste + $tweede;


		$bedrag2 = $bedrag - 5000;
		$kosten2 = $bedrag2 * 0.05;
		$kosten = $kosten + $kosten2;
		$derde = $kosten2;
	}
    if ($bedrag >= 10001 && $bedrag <= 190000) {
    	$eerste = 375;
    	$tweede = 250;
    	$derde = 250;
        $kosten = 375 + 250 + 250;


        $bedrag2 = $bedrag - 10000;
//        $bedrag = $bedrag - 5000;
        $kosten2 = $bedrag2 * 0.01;
        $kosten = $kosten + $kosten2;
        $vierde = $kosten2;
    }
    if ($bedrag >= 190001 && $bedrag <= 800000) {
    	$eerste = 375;
    	$tweede = 250;
    	$derde = 250;
    	$vierde = 1900;
        $kosten = 375 + 250 + 250 + 1900;


        $bedrag2 = $bedrag - 200000;
        $kosten2 = $bedrag2 * 0.005;
        $kosten = $kosten + $kosten2;
        $vijfde  = $kosten2;
    }
    if ($bedrag >= 800001) {
	    $eerste = 375;
	    $tweede = 250;
	    $derde = 250;
	    $vierde = 1900;
	    $vijfde = 4000;
		$kosten = 375 + 250 + 250 + 1900;

        $bedrag2 = $bedrag - 200000;
        $kosten2 = $bedrag2 * 0.005;
        $kosten = $kosten + $kosten2;
        $vijfde = $kosten2;

        if ($kosten > 6775) {
            $kosten = 6775;
            $vijfde = 4000;
        }
    }

	$kosten = round($kosten, 2);

	if ($btwPlichtig == "false") {
		$btw = $kosten * 0.21;
		$kosten = round($kosten + $btw, 2);
	}

	$dagen = $vandaag->diff($verval)->format("%a");

    $rente = $factuurkosten * $dagen * $rentePercentage / 365;

    $totaal =  $bedrag + $kosten + $rente;

	$bedrag = number_format((float)$bedrag, 2, ',', '.');
	$rente = number_format((float)$rente, 2, ',', '.');
	$kosten = number_format((float)$kosten, 2 , ',', '.');
	$totaal = number_format((float)$totaal, 2 , ',', '.');
    echo '<br/ > <hr />
    <div class="incassokosten">
        <div class="flex_column av_two_third  flex_column_div av-zero-column-padding el_before_av_two_third first avia-builder-el-first">
            <table style="width:100%">
                <tr>
                    <td>Oorspronkelijke factuur</td>
                    <td>€' . htmlspecialchars($bedrag) .  '</td>
                </tr>
                <tr>
                    <td><b>Totale incassokosten</b></td>
                    <td>€'. $kosten .  ' </td>
                </tr>
                <tr class="tablerow">
                    <td><div class="borderline">Rente</div></td>
                    <td>€' . $rente .  '</td>
                </tr>            
                <tr>
                    <td><b>Totale vordering</b></td>
                    <td>€' . $totaal .  '</td></td>
                </tr>
            </table> 
            <a href="/nu-incasseren/" class="avia-button avia-color-custom avia-size-large" style="background-color:#edae44; border-color:#edae44; color:#000000; "><span class="avia_iconbox_title"><strong>Nu incasseren</strong></span><span class="avia_button_icon avia_button_icon_right" aria-hidden="true"></span></a>
            <br /><br />
            <div class="table2">
                <b>Incassokosten</b>
                <p>De incassokosten zijn berekend aan de hand van onderstaande staffel:</p>
                <table style="width: 100%">                   
                    <tr><td>Over de eerste</td><td>€2.500,00</td><td>15%</td><td>€' . htmlspecialchars($eerste) .  '</td></tr>
                    <tr><td>Over de volgende</td><td>€2.500,00</td><td>10%</td><td>€' . htmlspecialchars($tweede) .  '</td></tr>
                    <tr><td>Over de volgende</td><td>€5.000,00</td><td>5%</td><td>€' . htmlspecialchars($derde) .  '</td></tr>
                    <tr><td>Over de volgende</td><td>€190.000,00</td><td>1%</td><td>€' . htmlspecialchars($vierde) .  '</td></tr>
                    <tr><td>Over de volgende</td><td>€800.000,00</td><td>0,5%</td><td>€' . htmlspecialchars($vijfde) . '</td></tr>
                </table>
            </div>
            <b>Rente</b>
            </br>
            <p class="centertext">De rente is berekend op basis van de wettelijke rente voor consumententransacties. 
            Houd er rekening mee dat deze berekening een benadering is! Het daadwerkelijke rentebedrag is afhankelijk van verschillende factoren,
            zoals het aantal facturen, deelbetalingen, algemene voorwaarden, etc.</p>            
        </div>        
        <div class="flex_column av_one_third  flex_column_div av-zero-column-padding el_after_av_one_third avia-builder-el-last">
            <h2 class="onze-aanpak">ONZE AANPAK</h2>
            <ul class="avia-icon-list avia-icon-list-left av-iconlist-big avia_animate_when_almost_visible avia_start_animation">
                <li class="avia_start_animation">
                    <div style="background-color:#b02b2c; border:1px solid #ffffff; color:#ffffff;" class="iconlist_icon avia-font-entypo-fontello">                        
                        <span class="iconlist-char" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></span>
                    </div>
                    <article class="article-icon-entry av-iconlist-empty" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="iconlist_content_wrap">
                            <header class="entry-content-header"><h4 class="iconlist_title" itemprop="headline" style="color:#333333; ">10 dagen</h4></header>
                            <div class="iconlist_content " itemprop="text"></div>
                        </div>
                        <footer class="entry-footer"></footer>
                    </article>
                    <div class="iconlist-timeline"></div>
                </li>
                <li class="avia_start_animation">
                    <div style="background-color:#b02b2c; border:1px solid #ffffff; color:#ffffff; "class="iconlist_icon avia-font-entypo-fontello">
                        <span class="iconlist-char" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></span>
                    </div>
                    <article class="article-icon-entry av-iconlist-empty" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="iconlist_content_wrap">
                            <header class="entry-content-header"><h4 class="iconlist_title" itemprop="headline" style="color:#333333; ">Incasseren</h4></header>
                            <div class="iconlist_content " itemprop="text"></div>
                        </div>
                        <footer class="entry-footer"></footer>
                    </article>
                    <div class="iconlist-timeline"></div>
                </li>
                <li class="avia_start_animation">
                    <div style="background-color:#b02b2c; border:1px solid #ffffff; color:#ffffff;" class="iconlist_icon avia-font-entypo-fontello">
                        <span class="iconlist-char" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></span>
                    </div>
                    <article class="article-icon-entry av-iconlist-empty" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="iconlist_content_wrap">
                            <header class="entry-content-header"><h4 class="iconlist_title" itemprop="headline"style="color:#333333; ">No cure no pay</h4></header>
                            <div class="iconlist_content " itemprop="text"></div>
                        </div>
                        <footer class="entry-footer"></footer>
                    </article>
                    <div class="iconlist-timeline"></div>
                </li>
                <li class="avia_start_animation">
                    <div style="background-color:#b02b2c; border:1px solid #ffffff; color:#ffffff;" class="iconlist_icon avia-font-entypo-fontello">
                        <span class="iconlist-char" aria-hidden="true" data-av_icon="" data-av_iconfont="entypo-fontello"></span>
                    </div>
                    <article class="article-icon-entry av-iconlist-empty" itemscope="itemscope" itemtype="https://schema.org/CreativeWork">
                        <div class="iconlist_content_wrap">
                            <header class="entry-content-header"><h4 class="iconlist_title" itemprop="headline" style="color:#333333; ">Geld op uw rekening</h4></header>
                            <div class="iconlist_content " itemprop="text"></div>
                        </div>
                        <footer class="entry-footer"></footer>
                    </article>
                    <div class="iconlist-timeline"></div>
                </li>
            </ul>
        </div>
    </div>';
}

?>

