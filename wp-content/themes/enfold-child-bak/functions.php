<?php

/*
* Add your own functions here. You can also copy some of the theme functions into this file.
* Wordpress will use those functions instead of the original functions then.
*/
//set builder mode to debug
/*
add_action('avia_builder_mode', "builder_set_debug");
function builder_set_debug()
{
	return "debug";
}
*/
//End set builder mode to debug

// WEBDELTA medewerker functienaliteit
if (!session_id()) {
    session_start();
}
if ($_GET['medewerker']) {
    $_SESSION['medewerker'] = strip_tags($_GET['medewerker']);
}
/*
incasso 			> medewerker ID = fld_7374118_1
meerdere incasso's 	> medewerker ID = fld_7851808_1
*/
function medewerker_func() {
    if (!empty($_SESSION['medewerker'])) {
        if ( strpos($_SERVER[REQUEST_URI], 'nu-incasseren') || strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen') ) {
            echo '<script>';
            echo 'var $j = jQuery.noConflict();$j(document).ready(function() {';
            if (strpos($_SERVER[REQUEST_URI], 'nu-incasseren')) {
                echo '$j("input[name=\'medewerker\']").val("'.$_SESSION['medewerker'].'");';
            }
            if (strpos($_SERVER[REQUEST_URI], 'meerdere-vorderingen')) {
                echo '$j("#fld_7851808_1").val("'.$_SESSION['medewerker'].'");';
            }
            echo '});';
            echo '</script>';
        }
    }
}
add_shortcode( 'medewerker', 'medewerker_func' );

function add_js_functions(){
    ?>
    <script>
        jQuery(document).ready(function($) {

            $('#vervaldatum').attr('autocomplete', 'off');

            $('#bereken').on( 'click' , function(e) {
                e.preventDefault();
                var min = 40;
                var max = 6675;

                var bedrag = $('#bedrag').val();
                var btwPlichtig = false;
                var date = $('#vervaldatum').val();
                var todayDate = new Date();
                var today = todayDate.toLocaleDateString();
                var debiteur = "";

                if ($('#bedrijf').is(':checked')) {
                    debiteur = "bedrijf";
                } else {
                    debiteur = "consument";
                }

                if ($('#btw').is(':checked')) {
                    btwPlichtig = true;
                }

                getAjax(bedrag, debiteur, btwPlichtig, date, today);
            });

            function getAjax(factuurBedrag, debiteur, btw, vervaldatum, vandaag) {
                $.ajax({
                    type:"POST",
                    url: "/wp-content/themes/enfold-child/berekenincasso.php",
                    data: {bedrag: factuurBedrag, debiteur: debiteur, btw: btw, vervaldatum: vervaldatum, vandaag: vandaag },
                    success: function(response) {
                        $("#outputBerekening").html(response);
                    }
                });
            }
        });



    </script>
    <?php
}
add_action('wp_head','add_js_functions');

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/css/custom.css',
        array('parent-style')
    );
} 